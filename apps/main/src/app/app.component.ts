import { Component } from '@angular/core';

@Component({
  selector: 'nx-redbarba-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
  notCoveredMethod(): void {
    const useless = 1;
    console.log('An useless value', useless);
  }
}
